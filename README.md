
## What this is:

This repo is just a collection of Linux & Mac BASH scripts that I use for different file operations and to make my life easier.

In here, you will find scripts that generate empty files from files listed in a logfile. Scripts that make using GPG easier and other things.

_I mostly work in Linux, so don't expect too many new Mac scripts._


## For Example:

cc.sh - Copy Comparison:<br>
This script takes two directories as input and checks each file with a checksum utility to make sure they match. This is useful for making sure backup copies are exactly alike.

rcc.sh - Recursive Copy Comparison:<br>
This script takes two directories as input and checks each file with a checksum utility to make sure they match. This is useful for making sure backup copies are exactly alike. Unlike Copy Comparison, this will go into sub-directories to make sure everything matches.


**More information can be found at the top of almost all of the scripts.**


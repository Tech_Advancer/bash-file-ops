
## WHAT THIS IS:

The GPG scripts let you quickly and easily encrypt, decrypt, sign, and verify GPG messages. For example, if you copy a GPG message, you can go to the command line and run "de" to decrypt the message. To encrypt a message, simply run:
en "message" toPerson1@mail.com toPerson2@mail.com


## INSTALL:


These scripts can be installed by following these steps:


1.   Go to your home folder and make a new folder named "bin".
2.   Now, go to the terminal and open ~/.bash_profile in your favorite editor. example: "sudo nano ~/.bash_profile"
3.   Add "export PATH=$PATH:~/bin"
4.   Save (For nano, hold ctrl and press "o". Then hold ctrl and press "x" to close.)
5.   Then add the files in this directory (en, de, sign, verify) to your bin folder.
6.   Be sure to change the example email address in en and sign from "derp@derp.derp" to your own email address!
7.   Then, in terminal, run "sudo chmod +x ~/bin/*".



## SCRIPTS:


en - encrypt:<br>
Encrypts a message and adds it to the clipboard. You will need to change the email in the script from "derp@derp.derp" to your GPG email address. 

Usage is:<br>
> en "message" recipient@mail.com recipient@derp.derp


de - decrypt:<br>
Decrypts a message that is currently in the clipboard. 

Usage is:<br>
> de


sign:<br>
Signs a message and adds it to the clipboard. You will need to change the email in the script from "derp@derp.derp" to your GPG email address. 

Usage is:<br>
> sign "message"


verify:<br>
Verifies a GPG message that is currently in the clipboard.

Usage is:<br>
> verify


## WHAT THIS IS:

This directory is for things that have no place anywhere else. These are just silly scripts that probably aren't too useful.


## INSTALL:

Scripts that don't end in ".sh" can be installed (made to be runnable from anywhere) by following these steps:


1.   Go to your home folder and make a new folder named "bin".
2.   Now, go to the terminal and open ~/.bash_profile in your favorite text editor. example: "sudo nano ~/.bash_profile"
3.   Add "export PATH=$PATH:~/bin"
4.   Save (For nano, hold ctrl and press "o". Then hold ctrl and press "x" to close.)
5.   Then add the files in this directory AND the clipboard script (one directory up) to your bin folder.
6.   Then, in terminal, run "sudo chmod +x ~/bin/*".

 
## SCRIPTS:
 
rot13:<br>
A script that applies rot13 to text. (Useful for trolling co-workers.)

> rot13 test

Returns: 
grfg

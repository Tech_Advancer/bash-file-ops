## WHAT THIS IS:

The GPG scripts let you quickly and easily encrypt, decrypt, sign, and verify GPG messages. For example, if you copy a GPG message to the clipboard, you can go to the command line and run "de" to decrypt the message. To encrypt a message, simply run:<br>
> en "message" toPerson1@mail.com toPerson2@mail.com


The encrypted message will automatically be sent to your clipboard so you can easily paste it wherever you like.


**More information on usage can be found at the top of almost all of the scripts.**


These all depend on the clipboard file, which is made for KDE. To make this work for other distros of Linux, you simply need to change the clipboard file to use your distro's clipboard tool. (The clipboard script is one directory up from this one.)


## INSTALL:

Scripts that don't end in ".sh" can be installed (made to be runnable from anywhere) by following these steps:


1.   Go to your home folder and make a new folder named "bin".
2.   Now, go to the terminal and open ~/.bash_profile in your favorite text editor. example: "sudo nano ~/.bash_profile"
3.   Add "export PATH=$PATH:~/bin"
4.   Save (For nano, hold ctrl and press "o". Then hold ctrl and press "x" to close.)
5.   Then add the files in this directory (de, en, sign, verify) AND the clipboard script (one directory up) to your bin folder.
6.   Be sure to change the example email address in en and sign from "derp@derp.derp" to your own email address!
7.   Then, in terminal, run "sudo chmod +x ~/bin/*".


If you already have a GPG key setup, you can start running with this instantly.


## SCRIPTS:

Most of these scripts depend on the clipboard script! Make sure it is in your ~/bin/ directory!


en - encrypt:<br>
Encrypts a message and adds it to the KDE clipboard. You will need to change the email in the script from "derp@derp.derp" to your GPG email address. [Uses the clipboard script.] 

Usage is:<br>
> en "message" recipient@mail.com recipient@derp.derp


de - decrypt:<br>
Decrypts a message that is currently in the KDE clipboard. [Uses the clipboard script.] 

Usage is:<br>
> de


sign:<br>
Signs a message and adds it to the KDE clipboard. You will need to change the email in the script from "derp@derp.derp" to your GPG email address. [Uses the clipboard script.] 

Usage is:<br>
> sign "message"


verify:<br>
Verifies a GPG message that is currently in the KDE clipboard. [Uses the clipboard script.] 

Usage is:<br>
> verify


endoc:<br>
Signs, encrypts, and timestamps any given file to a list of recipients that you put into the script. If you give it "document.pdf", it will output the encrypted "document.YEAR-MONTH-DAY.pdf.gpg" in the same directory.

Usage is:<br>
> endoc DocumentName.pdf

#!/bin/bash
################
# wipefiles.sh
################
#
# This script is made to be used with crontab. It
# clears all files from a directory AND SUB-directories 
# that are older than a given amount of minutes. 
#
# Just change the variables below!
#
###############
#
# Variables:
#
# TIME: How old, in minutes, the 
#   file should be before deleting it.
#
TIME=60
#
# DELDIR: The directory to delete old 
#   files from. 
#
DELDIR='/your/directory/to/delete/files/from'
#
# MAXDEPTH: The number of sub-directories 
#   down to go to search for files to delete.
#   1 is just the starting directory. 2 will 
#   look at that directory AND inside of 
#   folders in that directory. 3 will look 
#   inside of the sub-folder's sub-folders 
#   and so on.
#
MAXDEPTH=1
#
# LOGFILE: The file to log the wipe to.
#
LOGFILE='/var/log/directory_wipe.log'
#
###############


TIMESTAMP=$( date +"%Y-%m-%d_%H-%M-%S" )
echo ''  >> $LOGFILE
echo "[$TIMESTAMP] Crontab is starting wipefiles.sh." >> $LOGFILE

DELETE_COUNT=$(find $DELDIR -maxdepth $MAXDEPTH -type f  -mmin +$TIME -ls | wc -l)
echo "Files to delete: $DELETE_COUNT" >> $LOGFILE

# Delete ALL files older than TIME minutes:
find $DELDIR -maxdepth $MAXDEPTH -mmin +$TIME -type f -delete

echo 'wipefiles.sh. Is finished.' >> $LOGFILE

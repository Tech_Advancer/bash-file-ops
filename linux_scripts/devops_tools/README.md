## WHAT THIS IS:

This directory features scripts that are likely to be specific to IT or DevOps. 
 
 
## INSTALL:

Scripts that don't end in ".sh" can be installed (made to be runnable from anywhere) by following these steps:


1.   Go to your home folder and make a new folder named "bin".
2.   Now, go to the terminal and open ~/.bash_profile in your favorite text editor. example: "sudo nano ~/.bash_profile"
3.   Add "export PATH=$PATH:~/bin"
4.   Save (For nano, hold ctrl and press "o". Then hold ctrl and press "x" to close.)
5.   Then add the files in this directory AND the clipboard script (one directory up) to your bin folder.
6.   Then, in terminal, run "sudo chmod +x ~/bin/*".

 
## SCRIPTS:
 
version:<br>
This is just a script for lazy people to be able to easily get information about a package. It gives website links with information about the package. Honestly, this tool could be a lot smarter, but I'm too lazy to add to it right now.

Usage is:<br>
> version package-name


wipefiles.sh:<br>
This script clears all files from a directory AND SUB-directories that are older than a given amount of minutes. It's made to be used with crontab or some kind of automation. Variables can be changed in the script itself.



#!/bin/bash

##################
#
# Copy Comparison
#
# This script takes two directories as input and recursively checks
# each file with a checksum utility to make sure they match.
#
##################

# Check that any argument has been set:
[ $# -eq 0 ] && { echo "Usage is:"; echo "cc.sh \"/path/to/reference/dir\" \"/path/to/dir/to/check\" (optional)0=md5|1=sha1|2=sha256 (optional)0=failure-only|1=both"; exit 1; }

# Check that the required arguments are set:
if [ -z ${1+x} ]; then echo "directory 1 is unset"; exit 1; fi
if [ -z ${2+x} ]; then echo "directory 2 is unset"; exit 1; fi

# Check that the hashing algorithm is correct:
if [ -z ${3+x} ]; then ALGO=0; else if [ $3 -lt 3 ] && [ $3 -gt -1 ]; then ALGO=$3; else echo "Invalid algorithm. You must use 0 to 2 where 0 = md5, 1 = sha1, and 2 = sha256"; exit; fi fi

# Check that the reporting type is correct:
if [ -z ${4+x} ]; then REPORT=0; else if [ $4 -lt 2 ] && [ $4 -gt -1 ]; then REPORT=$4; else echo "Invalid reporting argument. You must use 0 or 1, where 0 = failure only and 1 = both success and failure"; exit; fi fi



# Check that argument 1 is a valid directory:
if [ -d "$1" ] && [ ! -L "$1" ]
then
    REF="$1";
else
    if [ -d "$PWD/$1" ] && [ ! -L "$PWD/$1" ]
    then
        REF="$PWD/$1";
    else
        echo "$1 is not a vaild directory.";
        exit;
    fi
fi

# Check that argument 2 is a valid directory:
if [ -d "$2" ] && [ ! -L "$2" ]
then
    CHECK="$2";
else
    if [ -d "$PWD/$2" ] && [ ! -L "$PWD/$2" ]
    then
        CHECK="$PWD/$2";
    else
        echo "$2 is not a vaild directory.";
        exit;
    fi
fi

# Strip trailing /:
REF=${REF%/};
CHECK=${CHECK%/};

echo "";
echo "Using";
echo "$REF";
echo "as a reference to check";
echo "$CHECK";
echo "";

find "$REF" -maxdepth 1 -mindepth 1 -type f | while read file;  #-printf '%h\0%d\0%p\n' | sort -t '\0' -n | awk -F '\0' '{print $3}' | while read file;
do
    LOOP_REF="$REF/${file##*/}";
    LOOP_CHECK="$CHECK/${file##*/}";

    if [ -f "$LOOP_CHECK" ] && [ -f "$LOOP_REF" ] && [ ! -L "$LOOP_CHECK" ]
    then
        case "$ALGO" in
        0)  REF_HASH=$(md5sum < "${LOOP_REF}")
            CHECK_HASH=$(md5sum < "${LOOP_CHECK}")
            ;;
        1)  REF_HASH=$(sha1sum < "${LOOP_REF}")
            CHECK_HASH=$(sha1sum < "${LOOP_CHECK}")
            ;;
        2)  REF_HASH=$(sha256sum < "${LOOP_REF}")
            CHECK_HASH=$(sha256sum < "${LOOP_CHECK}")
            ;;
        esac

        if [ "$REF_HASH" = "$CHECK_HASH" ];
        then
            # Files match; good result
            if [ $REPORT -eq 1 ]
            then
                echo "$LOOP_CHECK matches.";
            fi
            continue;
        else
            if [ "$LOOP_REF" -nt "$LOOP_CHECK" ];
            then
                # REF is a newer file than check
                echo "$LOOP_CHECK hash mismatch! The file is old compared to the reference.";
                continue;
            else
                echo "$LOOP_CHECK hash mismatch! The file has been changed or corrupted.";
                continue;
            fi
        fi
    else
        if [ -d "$LOOP_CHECK" ] && [ -d "$LOOP_REF" ] && [ ! -L "$LOOP_CHECK" ]
        then
		# to check multiple directories, see rcc.sh (Recursive Copy Comparison)
		continue;
        else
            if  [ ! -L "$LOOP_CHECK" ]
            then
                echo "$LOOP_CHECK does NOT exist!";
                continue;
            fi
        fi
    fi
done

echo "";
echo "==Comparison complete==";
echo "";

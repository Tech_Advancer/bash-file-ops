## WHAT THIS IS:

This directory features scripts that could be used by anyone. (Not just Dev, IT, or DevOps.) 
 

**More information can be found at the top of almost all of the scripts.**


cc.sh - Copy Comparison:<br>This script takes two directories as input and checks each file with a checksum utility to make sure they match. This is useful for making sure backup copies are exactly alike.

Usage is:<br>
> rcc.sh "/path/to/reference/dir" "/path/to/dir/to/check" (optional)0=md5|1=sha1|2=sha256 (optional)0=failure-only|1=both


rcc.sh - Recursive Copy Comparison:<br>This script takes two directories as input and checks each file with a checksum utility to make sure they match. This is useful for making sure backup copies are exactly alike. Unlike Copy Comparison, this will go into sub-directories to make sure everything matches.

Usage is:<br>
> rcc.sh "/path/to/reference/dir" "/path/to/dir/to/check" (optional)0=md5|1=sha1|2=sha256 (optional)0=failure-only|1=both


**More information can be found at the top of almost all of the scripts.**

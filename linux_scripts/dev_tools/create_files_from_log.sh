#!/bin/bash

##################
#
# Reads in file names from a text file, seperated by newline, and creates them.
#
# WARNING: This will overwrite any already-existing files!
#
# Text file example:
# doc_123.pdf
# doc_456.pdf
# doc_789.pdf
#
##################

# Check that any argument has been set:
[ $# -eq 0 ] && { echo "Usage is:"; echo "create_files_from_log.sh \"file_with_filenames.txt\" \"/path/to/create/files/in/\" (optional)1=verbose_output"; exit 1; }

# Set logfile and strip trailing /:
LOGFILE="$1";
LOGFILE=${LOGFILE%/};
CREATEPATH="$2";
CREATEPATH=${CREATEPATH%/};
if [ -z ${3+x} ]; then VERBOSE=0; else VERBOSE=1; fi

if [ ! -f "$LOGFILE" ]; then echo "Logfile not found!"; exit 1; fi
if [ ! -d "$CREATEPATH" ]; then echo "Path not valid!"; exit 1; fi

if [ $VERBOSE -eq 1 ]; then echo ""; echo "Creating files..."; fi

# Read in logfile line by line:
while IFS='' read -r line || [[ -n "$line" ]]; do
    # create an empty file with the name of the line
    echo "" > "$CREATEPATH/${line}"
    if [ $VERBOSE -eq 1 ]; then echo "${line}"; fi
done < "$LOGFILE"

echo "";
echo "Creation Complete";
echo "";

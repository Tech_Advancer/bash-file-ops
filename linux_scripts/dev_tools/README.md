## WHAT THIS IS:

This directory features scripts that are likely to be specific to developers. 
 
 
## INSTALL:

Scripts that don't end in ".sh" can be installed (made to be runnable from anywhere) by following these steps:


1.   Go to your home folder and make a new folder named "bin".
2.   Now, go to the terminal and open ~/.bash_profile in your favorite text editor. example: "sudo nano ~/.bash_profile"
3.   Add "export PATH=$PATH:~/bin"
4.   Save (For nano, hold ctrl and press "o". Then hold ctrl and press "x" to close.)
5.   Then add the files in this directory AND the clipboard script (one directory up) to your bin folder.
6.   Then, in terminal, run "sudo chmod +x ~/bin/*".

 
## SCRIPTS:
 
pjson:<br>
This script will take any valid JSON string that you have in your clipboard, format it to be easy to read, and open it in a KDE Kate text editor. (You can easily change it to another editor in the script.) [Uses the clipboard script.] 

Usage is:<br>
> pjson


create_files_from_log.sh:<br>
This script lets you take a text file full of file names (seperated by a newline) and create empty files with the file names listed in that file. (This is useful for testing what went wrong with certain logfiles.) This script still needs a better name.

Usage is:<br>
> create_files_from_log.sh "file_with_filenames.txt" "/path/to/create/files/in/" (optional)1=verbose_output



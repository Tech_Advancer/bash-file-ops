## WHAT THIS IS:

Everything in this directory was made to work on Linux only. (Specifically BASH on Debian based systems.)

Scripts in this directory may be dependencies for other scripts in sub-folders. (Thus allowing you to easily swap dependencies.)


## INSTALL:

Scripts that don't end in ".sh" can be installed (made to be runnable from anywhere) by following these steps:


1.   Go to your home folder and make a new folder named "bin".
2.   Now, go to the terminal and open ~/.bash_profile in your favorite text editor. example: "sudo nano ~/.bash_profile"
3.   Add "export PATH=$PATH:~/bin"
4.   Save (For nano, hold ctrl and press "o". Then hold ctrl and press "x" to close.)
5.   Then add the files in these sub-directories AND the clipboard script to your bin folder.
6.   Then, in terminal, run "sudo chmod +x ~/bin/*".

 
## SCRIPTS:
 
clipboard:<br>
A utility script that copies data to the KDE 4 clipboard. This is used by the other scripts instead of calling the clipboard directly. That way, you can change this to make the other scripts use a different clipboard tool. You probably won't call this directly.
